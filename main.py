from console.menus import printTitle, mainMenu, readNumber, onGameMenu, createMenu
from model.MineSweeper import MineSweeper, Level

def createCustomGame()->MineSweeper:
    printTitle("Create Custom Game")
    fil   = readNumber("Fil...", 5, 25)
    col   = readNumber("Col...", 5, 25)
    bombs = readNumber("Boms..", 1, fil*col/2)
    return MineSweeper(Level.CUSTOM,fil,col,bombs)

def playGame(game:MineSweeper):
    isExit=False
    while game.onGame and not isExit:
        printTitle("MineSweeper")
        print(game)
        option = onGameMenu()
        if option == 1:
            fil = readNumber("Fil...", 1, game.board.heigth)
            col = readNumber("Col...", 1, game.board.width)
            game.play(fil-1,col-1)
            if not game.onGame:
                print(game)
        if option == 2:
            fil = readNumber("Fil...", 1, game.board.heigth)
            col = readNumber("Col...", 1, game.board.width)
            game.addFlag(fil-1,col-1)
        if option == 3:
            fil = readNumber("Fil...", 1, game.board.heigth)
            col = readNumber("Col...", 1, game.board.width)
            game.removeFlag(fil-1,col-1)
        elif option == 0:
            isExit = True
        else:
            print("Not valid option")

def createGame():
    game: MineSweeper = None
    isExit = False

    while not isExit:
        option = createMenu()
        if option == 1:
            game = MineSweeper(Level.EASY)
            playGame(game)
            isExit = True
        elif option == 2:
            game = MineSweeper(Level.MEDIUM)
            playGame(game)
            isExit = True
        elif option == 3:
            game = MineSweeper(Level.HARD)
            playGame(game)
            isExit = True
        elif option == 4:
            game = createCustomGame()
            playGame(game)
            isExit = True
        elif option == 0:
            isExit = True
        else:
            print("Not valid option")

"""
+----------------------------------------------+
| MAIN....
+----------------------------------------------+
"""
isExit = False
while not isExit:
    option = mainMenu()
    if option == 1:
        createGame()
    elif option == 0:
        isExit = True
        print("Thanks to use our application")
    else:
        print("Not valid option")


