from model.MineSweeper import MineSweeper

def readNumber(comment:str, min:int, max:int):
    invalid = True
    option = -1
    while invalid:
        option = int(input(comment+"> "))
        if option < min or option > max:
            print("Wrong value...")
        else:
            invalid = False
    return option

def printTitle(title:str):
    print("+-------------------------------------+")
    print(f"|{title}|")
    print("+-------------------------------------+")

def mainMenu():
    printTitle("Main menu")
    print("1.- Play Game")
    print("0.- Exit")
    option = int(input("> "))
    return option

def createMenu():
    printTitle("Create Game")
    print("1.- Easy")
    print("2.- Medium")
    print("3.- Hard")
    print("4.- Custom")
    print("0.- Back")
    option = int(input("> "))
    return option

def onGameMenu():
    print("1.- Uncover")
    print("2.- Add flag")
    print("3.- Remove flag")
    print("0.- Exit game")
    option = int(input("> "))
    return option