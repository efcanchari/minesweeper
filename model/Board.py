from random import randrange
from model.Cell import Cell

class Board:
    def __init__(self,width=10,heigth=10,bombs=8):
        self.__width=width
        self.__heigth=heigth
        self.__bombs=bombs
        self.__uncovered=width*heigth
        self.__matrix=[]
        self.__initialize(width,heigth,bombs)
        self.__exploded=False

    @property
    def width(self):
        return self.__width

    @property
    def heigth(self):
        return self.__heigth

    @property
    def bombs(self):
        return self.__bombs

    @property
    def uncovered(self):
        return self.__uncovered

    def __initialize(self,width:int,heigth:int,bombs:int):
        #Create  NxM matrix
        cont = 0
        for fil in range(heigth):
            rowCells=[]
            for col in range(width):
                rowCells.append(Cell())
            self.__matrix.append(rowCells)

        #Set Random Bombs
        count=0
        while count<bombs:
            fil = randrange(heigth)#row
            col = randrange(width)#col
            cell = self.__matrix[fil][col]
            if cell.isBomb()==False:
                cell.setBomb()
                count += 1

        #Set Bomb Counters
        for fil in range(heigth):
            for col in range(width):
                #print(f"({fil},{col})")
                self.__setBombCounters(fil,col)

    def __setBombCounters(self, fil:int,col:int):
        cell = self.__matrix[fil][col]
        if cell.isBomb():
           cell.setCountBombs(-1)
        else:
            count = 0
            for i in range(fil - 1, fil + 2):
                for j in range(col - 1, col + 2):
                    if not self.isValidCoord(i,j):
                        pass
                    elif i==fil and j== col:
                        pass
                    elif self.__matrix[i][j].isBomb():
                        count += 1
            cell.setCountBombs(count)

    def isValidCoord(self, fil:int, col:int):
        if fil<0 or fil>=self.__heigth:
            return False
        elif col<0 or col>=self.__width:
            return False
        else:
            return True

    def __str__(self):
        strBoard = "   "
        for col in range(self.__width):
            strBoard += str((col+1)%10)
            if col < self.__width - 1:
                strBoard += "|"
        strBoard +="\n"

        for fil in range(self.__heigth):
            strBoard += str((fil+1)%10)+" ["
            for col in range(self.__width):
                if self.__exploded:
                    strBoard += self.__matrix[fil][col].uncoverString()
                else:
                    strBoard += str(self.__matrix[fil][col])
                if col < self.__width-1:
                    strBoard += "|"
            strBoard += "]"
            if fil < self.__heigth - 1:
                strBoard += "\n"
        return strBoard



    def uncover(self,fil:int,col:int):
        cell = self.__matrix[fil][col]
        if cell!=None:
            cell.uncover()
            self.__uncovered -= 1
            if cell.isBomb():
                self.__exploded=True
            elif cell.countBombs()==0:
                self.__uncoverEmpty(fil,col)

    def __uncoverEmpty(self,fil:int,col:int):
        for i in range(fil-1,fil+2):
            for j in range(col-1,col+2):
                if self.isValidCoord(i,j):
                    cell = self.__matrix[i][j]
                    if cell._bomb==False and cell._covered:
                        cell.uncover()
                        self.__uncovered -= 1
                        if i!=fil and j!=col and cell.countBombs()==0:
                            self.__uncoverEmpty(i,j)

    @property
    def exploted(self):
        return self.__exploded

    @property
    def remaining(self):
        return self.__width * self.__heigth - self.__uncovered

    def addFlag(self, fil:int, col:int):
        self.__matrix[fil][col].flag()

    def removeFlag(self, fil:int, col:int):
        self.__matrix[fil][col].unflag()



