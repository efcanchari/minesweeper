from enum import Enum

from model.Board import Board


class Level(Enum):
    EASY = 1
    MEDIUM = 2
    HARD = 3
    CUSTOM = 4

class MineSweeper:
    def __init__(self, level:Level,width=10,heigth=10,bombs=10):
        self.__onGame = True
        self.__board = None
        if level==Level.EASY:
            self.__board = Board(8,8,10)
        elif level==Level.MEDIUM:
            self.__board = Board(16,16,40)
        elif level==Level.HARD:
            self.__board = Board(30,16,60)
        else:
            self.__board = Board(width, heigth, bombs)

    @property
    def board(self)->Board:
        return self.__board

    @property
    def onGame(self):
        return self.__onGame


    def __str__(self):
        strGame = ""
        strGame += f"Width....: {self.__board.width}\n"
        strGame += f"Heigth...: {self.__board.heigth}\n"
        strGame += f"Bombs....: {self.__board.bombs}\n"
        strGame += f"Uncovered: {self.__board.uncovered}\n"
        strGame += f"{str(self.__board)}"
        return strGame

    def play(self,fil:int,col:int):
        if self.__board.isValidCoord(fil,col):
            self.__board.uncover(fil,col)
            if self.__board.exploted:
                print( "You Lost! ¯\_(ツ)_/¯" )
                self.__onGame = False
            elif self.__board.bombs==self.__board.remaining:
                print("You Won!! \ (•◡•) /")
                self.__onGame = False
            else:
                print("Continue Playing")

    def addFlag(self,fil:int,col:int):
        if self.__board.isValidCoord(fil, col):
            self.__board.addFlag(fil, col)

    def removeFlag(self,fil:int,col:int):
        if self.__board.isValidCoord(fil, col):
            self.__board.removeFlag(fil, col)

