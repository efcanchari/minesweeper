class Cell:
    def __init__(self,isCovered=True,isBomb=False,countBombs=0):
        self._covered:bool = isCovered
        self._bomb: bool = isBomb
        self._countBombs:int = countBombs
        self._flag:bool = False

    def isCovered(self):
        return self._covered

    def isBomb(self):
        return self._bomb

    def countBombs(self):
        return self._countBombs

    def uncover(self):
        self._covered=False

    def setBomb(self):
        self._bomb=True

    def setCountBombs(self,countBombs:int):
        self._countBombs=countBombs

    def flag(self):
        self._flag=True

    def unflag(self):
        self._flag=False

    def uncoverString(self)->str:
        if self._flag:
            return "⚑"
        elif self._bomb:
            return "*"
        elif self._covered:
            return "□"
        elif self._countBombs==0:
            return " "
        else:
            return str(self._countBombs)

    def __str__(self):
        if self._flag:
            return "⚑"
        elif self._covered:
            return "□"
        elif self._bomb:
            return "*"
        elif self._countBombs==0:
            return " "
        else:
            return str(self._countBombs)