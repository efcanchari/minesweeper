##Minesweeper model


*Cell*
- status: covered/uncovered
- bom: ture/false
- bombs counter

*Board*
````
|1|Q|_|
|_|2|Q|
|_|1|_|
````
- CellxCell matrix
- width and height
- uncovered cells count
- total bombs number